import { IRepositoryChambres } from "../Domain/IRepositoryChambres";
import {Chambre} from "../Domain/Chambre"
import { ReccupererChambresUseCase } from "./ReccupererChambresUseCase";

describe('Reccuperer chambre use case', () => {
    
    let repositoryChambre: IRepositoryChambres;
    let reccupererChambresUseCase: ReccupererChambresUseCase;
    beforeEach(()=> {
        repositoryChambre = new ReccupererChambresMock()
        reccupererChambresUseCase = new ReccupererChambresUseCase(repositoryChambre);
    }) 
    it('Reccuperer les chambres', () => {
        // GIVEN
        
        const expectedChambre = new Chambre(2, 2, "test", 2)
        // WHEN
        const chambres = reccupererChambresUseCase.handle()
        // THEN
        expect(repositoryChambre.reccuperer).toBeCalled
    });


})


class ReccupererChambresMock implements IRepositoryChambres{

    reccuperer(): Chambre[]{

        return [new Chambre(2, 2, "test", 2)]
    }
}