import { IRepositoryChambres } from "../Domain/IRepositoryChambres";
import { Chambre } from "../Domain/Chambre";
import { IReccupererChambreUseCase } from "./IReccupererChambreUseCase";



export class ReccupererChambresUseCase implements IReccupererChambreUseCase {


    constructor(private repositoryChambre: IRepositoryChambres) {
    }

    handle(): Chambre[] {
        const chambres = this.repositoryChambre.reccuperer();
        return chambres;
    }

}
