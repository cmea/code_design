import { Chambre } from "../Domain/Chambre";
 

export interface IReccupererChambreUseCase{
    handle(): Chambre[]
}