"use strict";
exports.__esModule = true;
exports.ReccupererChambresUseCase = void 0;
var ReccupererChambresUseCase = /** @class */ (function () {
    function ReccupererChambresUseCase(repositoryChambre) {
        this.repositoryChambre = repositoryChambre;
    }
    ReccupererChambresUseCase.prototype.handle = function () {
        var chambres = this.repositoryChambre.reccuperer();
        return chambres;
    };
    return ReccupererChambresUseCase;
}());
exports.ReccupererChambresUseCase = ReccupererChambresUseCase;
