import { IRepositoryChambres } from "../../Domain/IRepositoryChambres";
import {Chambre} from "../../Domain/Chambre"
import { ReccupererChambres } from "./ReccupererChambreRepository";

describe('Chambre repository', () => {
    
    let repositoryChambre: IRepositoryChambres;
 
    it('Test chambre repository', () => {
        // GIVEN
        repositoryChambre = new ReccupererChambres()

        // WHEN
        const chambres = repositoryChambre.reccuperer()
        // THEN
        expect(chambres).toHaveLength(12) 

    });

})

