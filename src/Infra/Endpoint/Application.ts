import { IReccupererChambreUseCase } from "../../UseCases/IReccupererChambreUseCase";
import {ReccupererChambresUseCase } from "../../UseCases/ReccupererChambresUseCase";
import {ReccupererChambres } from "../../Infra/Repository/ReccupererChambreRepository";



export class Application{
    constructor(private reccupererChambresUseCase: IReccupererChambreUseCase){
        
    };

    afficherChambre(){
        const chambres = this.reccupererChambresUseCase.handle()
        //
        const affichageChambres = chambres.map(function(chambre, index){
            return {"etage": chambre.etage, "numero": chambre.numero, "description": chambre.description, "capacite": chambre.capacite + " guests"};
          })
        console.table(affichageChambres)

    }
}


const repositoryChambre = new ReccupererChambres()
const reccupererChambresUseCase = new ReccupererChambresUseCase(repositoryChambre);
const app = new Application(reccupererChambresUseCase)
app.afficherChambre()