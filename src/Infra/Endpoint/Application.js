"use strict";
exports.__esModule = true;
exports.Application = void 0;
var ReccupererChambresUseCase_1 = require("../../UseCases/ReccupererChambresUseCase");
var ReccupererChambreRepository_1 = require("../../Infra/Repository/ReccupererChambreRepository");
var Application = /** @class */ (function () {
    function Application(reccupererChambresUseCase) {
        this.reccupererChambresUseCase = reccupererChambresUseCase;
    }
    ;
    Application.prototype.afficherChambre = function () {
        var chambres = this.reccupererChambresUseCase.handle();
        //
        var affichageChambres = chambres.map(function (chambre, index) {
            return { "etage": chambre.etage, "numero": chambre.numero, "description": chambre.description, "capacite": chambre.capacite + " guests" };
        });
        console.table(affichageChambres);
    };
    return Application;
}());
exports.Application = Application;
var repositoryChambre = new ReccupererChambreRepository_1.ReccupererChambres();
var reccupererChambresUseCase = new ReccupererChambresUseCase_1.ReccupererChambresUseCase(repositoryChambre);
var app = new Application(reccupererChambresUseCase);
app.afficherChambre();
