"use strict";
exports.__esModule = true;
exports.Etage = exports.Chambre = void 0;
var Chambre = /** @class */ (function () {
    function Chambre(etage, numero, description, capacite) {
        this.etage = etage;
        this.numero = numero;
        this.description = description;
        this.capacite = capacite;
    }
    return Chambre;
}());
exports.Chambre = Chambre;
var Etage = /** @class */ (function () {
    function Etage(value) {
    }
    return Etage;
}());
exports.Etage = Etage;
