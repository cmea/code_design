import {Chambre} from "./Chambre"

export interface IRepositoryChambres{

    reccuperer(): Chambre[];
}