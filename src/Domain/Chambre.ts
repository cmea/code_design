
export class Chambre{
    etage: number;
    numero: number;
    description: string;
    capacite: number;

    constructor(etage: number, numero: number, description: string, capacite: number){
        this.etage = etage;
        this.numero = numero;
        this.description = description;
        this.capacite = capacite;

    }
}



